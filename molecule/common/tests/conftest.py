import json
import os
import pytest
import yaml

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('zabbix_server')

@pytest.fixture
def ansible_vars():
    pth = os.path.join(os.path.dirname(__file__), '..', 'converge.yml')
    with open(pth) as ymlfile:
        ansible_vars = yaml.safe_load(ymlfile)[1]['roles'][0]['vars']

    return ansible_vars


@pytest.fixture
def zabbix_agent_version(ansible_vars):
    zabbix_agent_version = ansible_vars["zabbix_agent_version"]

    return zabbix_agent_version


@pytest.fixture
def zabbix_auth(host, ansible_vars):

    encoded_body = json.dumps(
        {
            "jsonrpc": "2.0",
            "method": "user.login",
            "id": 1,
            "auth": None,
            "params": {
                "user": ansible_vars["zabbix_api_user"],
                "password": ansible_vars["zabbix_api_pass"]
            }
        }
    )

    response = host.run(
            f"curl -s http://localhost/api_jsonrpc.php "
            f"-H 'Content-Type: application/json-rpc' -X POST "
            f"--data {json.dumps(encoded_body)}"
            )
    return json.loads(response.stdout)['result']

@pytest.fixture
def get_hosts():
    return [
        "zabbix-agent-centos",
    ]
