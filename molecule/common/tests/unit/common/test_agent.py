import os
from zabbix_api import ZabbixAPI

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('zabbix_agent')


def test_agent_version(host, zabbix_agent_version):
    assert host.package("zabbix-agent").version.startswith(zabbix_agent_version)
