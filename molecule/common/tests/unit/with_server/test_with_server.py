import json
import os
import pytest

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('zabbix_server')

def get_hosts():
    return [
        "zabbix-agent-centos",
    ]

def test_hosts(host, zabbix_auth):
    hosts = get_hosts()

    encoded_body = json.dumps(
        {
            "jsonrpc": "2.0",
            "method": "host.get",
            "params": {
                "filter": {
                    "host": hosts
                }
            },
            "auth": zabbix_auth,
            "id": 1
        }
    )

    response = host.run(
        f"curl localhost/api_jsonrpc.php "
        f"-H 'Content-Type: application/json-rpc' -X POST "
        f"--data {json.dumps(encoded_body)}"
        )

    result = json.loads(response.stdout)['result']

    for server in result:
      assert server['name'] in hosts

def test_hosts_status(host, zabbix_auth):
    hosts = get_hosts()

    encoded_body = json.dumps(
        {
            "jsonrpc": "2.0",
            "method": "host.get",
            "params": {
                "filter": {
                    "host": hosts
                }
            },
            "auth": zabbix_auth,
            "id": 1
        }
    )

    response = host.run(
        f"curl localhost/api_jsonrpc.php "
        f"-H 'Content-Type: application/json-rpc' -X POST "
        f"--data {json.dumps(encoded_body)}"
        )

    result = json.loads(response.stdout)['result']


    for server in result:
        if server['name'] != 'Zabbix server':
            assert int(server['status']) == 0
